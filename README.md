# JAliEn - User Setup

## Setup certificate

[Reference](https://ca.cern.ch/ca/Help/?kbid=024010)

```
mkdir ~/.globus
sudo chmod 700 ~/.globus
openssl pkcs12 -in cert.p12 -clcerts -nokeys -out ~/.globus/usercert.pem
openssl pkcs12 -in cert.p12 -nocerts -out ~/.globus/userkey.pem
sudo chmod 600 ~/.globus/user{cert,key}.pem
```

## Install CVMFS

[Reference](https://cvmfs.readthedocs.io/en/stable/cpt-quickstart.html)

```
wget https://ecsft.cern.ch/dist/cvmfs/cvmfs-release/cvmfs-release-latest_all.deb
sudo dpkg -i cvmfs-release-latest_all.deb
rm -f cvmfs-release-latest_all.deb
sudo apt update
sudo apt install cvmfs
sudo apt install environment-modules
```

## Setup CVMFS

```
sudo cvmfs_config setup
# check if /etc/auto.master.d/cvmfs.autofs contains /cvmfs /etc/auto.cvmfs
# Create default local
# https://alien.web.cern.ch/content/documentation/howto/site/setupcvmfs
# Create the following file with the following content
cat /etc/cvmfs/default.local
CVMFS_REPOSITORIES=alice.cern.ch
CVMFS_CLIENT_PROFILE=single
CVMFS_HTTP_PROXY=DIRECT
CVMFS_QUOTA_LIMIT=20000
```

## Magic

```
sudo cvmfs_config setup
sudo service autofs start
cvmfs_config probe
sudo cvmfs_config chksetup
```

**Note:** For Fedora 22 - you must downgrade `tcl`/install `tcl 8.5`.


## Run

```
/cvmfs/alice.cern.ch/bin/alienv enter xjalienfs
[xjalienfs] ~ > help
...
[xjalienfs] ~ > alien.py
Welcome to the ALICE GRID
```

## Run a job

### Local - create a JDL file

```
cat test.jdl
Executable = "/alice/cern.ch/user/m/mariaele/testscript.sh";
Output = {stdout@disk=1};
OutputDir = "/alice/cern.ch/user/m/mariaele/output_dir_new/";
WorkDirectorySize = {
	"11000MB"
};
```

### Local - create script

```
cat testscript.sh
#!/bin/bash

echo "Hello!"
```

### Connect to alien

```
/cvmfs/alice.cern.ch/bin/alienv enter xjalienfs
```

### Copy files

```
[xjalienfs] ~ > alien.py cp file:jdl/test.jdl "~/test.jdl"
[xjalienfs] ~ > alien.py cp file:jdl/testscript.sh "~/testscript.sh"
```

### Run alien.py

```
[xjalienfs] ~ > alien.py
Welcome to the ALICE GRID

AliEn[<username>]:/alice/cern.ch/user/u/<username>/ > submit test.jdl
```

### Verify job from alimonitor.cern.ch -> myJobs or

```
AliEn[<username>]:/alice/cern.ch/user/u/<username>/ > ps
```

## Local setup

```
git clone https://gitlab.cern.ch/jalien/jalien
git clone https://gitlab.cern.ch/jalien/jalien-setup
```

### Compile jalien

```
cd jalien
./compile.sh cs
```

### Setup jalien

```
cd jalien-setup
make all
./bin/jared --jar ~/jalien/alien-cs.jar --volume ~/volume
```

### Setup env

#### Edit /etc/hosts and add:

```
127.0.0.1	JCentral-dev
127.0.0.1	JCentral-dev-SE
```

#### Start containers

```
cd ~/volume
docker-compose up
```

#### Source things (other terminal)

```
source /cvmfs/alice.cern.ch/etc/login.sh
cd ~/volume
source env_setup.sh
alienv enter xjalienfs
source env_setup.sh
alien.py
```

#### Start optimizer

```
docker exec -it volume_JCentral-dev_1 /bin/bash
cd jalien-dev
./optimiser.sh
```

### Copy test.jdl and testscript.sh

#### File content

```
[local@cern jdl]$ cat testscript.sh
#!/bin/bash

echo "Hello!"
[local@cern jdl]$ cat test.jdl
Executable = "/localhost/localdomain/user/j/jalien/testscript.sh";
Output = {stdout@disk=1};
OutputDir = "/localhost/localdomain/user/j/jalien/output_dir_new/";
WorkDirectorySize = {
	"11000MB"
};
```

#### Copy files

```
[xjalienfs] volume > alien.py cp file:jdl/test.jdl "test.jdl"
jobID: 1/1 >>> Start
jobID: 1/1 >>> STATUS OK >>> SPEED 2.11 KiB/s
Succesful jobs (1st try): 1/1
[xjalienfs] volume > alien.py cp file:jdl/testscript.sh "testscript.sh"
jobID: 1/1 >>> Start
jobID: 1/1 >>> STATUS OK >>> SPEED 4.05 KiB/s
Succesful jobs (1st try): 1/1
```

#### Submit job

```
alien.py
submit test.jdl
```

#### Unlock queues

```
docker exec -it volume_JCentral-dev_1 /bin/bash
root@JCentral-dev:/# mysql --verbose --host=127.0.0.1 --port=3307 --password=pass --user=root
use processes;
update SITEQUEUES set blocked="open", status="new" where siteID=1;
```

